<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'git-project3' );

/** MySQL database username */
define( 'DB_USER', 'git-project3' );

/** MySQL database password */
define( 'DB_PASSWORD', 'git-project3' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '-+<w5?wh|>J5+=[Ie7|`aZpBupn[cN 7ci.*eA:8FaF(FWF!;xsKR(S1A( D.jtn' );
define( 'SECURE_AUTH_KEY',  '$Pl65L#=a{ia}pa,;tB_{BIjKT?Dk?^w18&h2KAAc=4hVIqd7s]PKU.&>^,jRrdY' );
define( 'LOGGED_IN_KEY',    'y~*1S]lc63sVuFV5:Xtr@8IVOIyj9QYfa9xBk)#JY%,+ETFM34.->:IiUi;p6H5Y' );
define( 'NONCE_KEY',        '.]t_QH;JglKdAqZ/GsQxhy{swLqe3Jo1d?-rH5.nF;GI}yj8[js-gC8!A|`$H!4^' );
define( 'AUTH_SALT',        'I.i^E[v]wdn4CX#GCx0?vo.$jhr0$o6Hl&x68_@ykRe}tE49VtA|Y_XX>{l~-tE&' );
define( 'SECURE_AUTH_SALT', 'VK&tA|{it*IJkUKuR.rFI`A8 c793/W^~Z??y2HD/+T8P||dw/.4-O];FqrMVrC}' );
define( 'LOGGED_IN_SALT',   '(!0m9bQ)sYIE%!eKo3J>y*wX_3p6gn&)d!cF#Kf8s1N6vV-au+M4bd^)@n7rgrN+' );
define( 'NONCE_SALT',       't$0DaRU~<N]*%tGJckh^d.I}dI34mA!W?sXc_A;{y|0VEgl3~.q.u1+S#}+{y){L' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
